package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import itmo.project.Sound.ButtonSound;
import itmo.project.Sound.Sound;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class MainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button InfoButton;

    @FXML
    private Button MineTextButton;

    @FXML
    private Button MiniGamesButton;

    @FXML
    private Button RandomTextButton;

    @FXML
    private Button StatsButton;


    @FXML
    void initialize() {

        InfoButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            InfoButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("info.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        RandomTextButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            RandomTextButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("RandomTextMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        MineTextButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            MineTextButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MineText.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        MiniGamesButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            MiniGamesButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MiniGames.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        StatsButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            StatsButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Stats.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });

      }

    }


