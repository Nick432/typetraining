package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Timer;

import itmo.project.Sound.ButtonSound;
import itmo.project.Texts.SettingsOfTexts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import static itmo.project.Texts.SettingsOfTexts.SetText;

public class MainPlaySceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button EndButton;

    @FXML
    private TextArea InputArea;

    @FXML
    private TextArea OutputArea;

    @FXML
    private Button ReturnButton;

    @FXML
    void initialize() {

        InputArea.setText("");


        long time = System.currentTimeMillis(); //время начала


        ReturnButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            ReturnButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));



            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        //Закончить текст

        EndButton.setOnAction(event -> {
            ResultsController.time = (System.currentTimeMillis() - time)/ 1000; // Отправка времени писания текста в секундах
            ButtonSound.playMelody();  //звук кнопки
            ResultsController.output = OutputArea.getText();

            if (!(InputArea.getText().equals(" ") || InputArea.getText().equals(""))) {
                ResultsController.input = InputArea.getText();
            } else {
                ResultsController.input = "null";
            }


            EndButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Results.fxml"));



            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        //Закончить текст

        // Загрузка текста
        String output = " ";

        switch (SettingsOfTexts.typeOfText) {//Проверка какой код текста

            case 0 -> { //Если код 0 то нажата кнопка с малым текстом
                output = SetText(0);  //Вызов функции из SizeOfRandomText.java, чтобы получить рандомный текст
                OutputArea.setText(output);     //Вставляю текст в area
            }
            case 1 -> { //Если код 1 то нажата кнопка со средним текстом
                output = SetText(1);  //Вызов функции из SizeOfRandomText.java, чтобы получить рандомный текст
                OutputArea.setText(output);     //Вставляю текст в area
            }
            case 2 -> { //Если код 2 то нажата кнопка с большим текстом
                output = SetText(2);  //Вызов функции из SizeOfRandomText.java, чтобы получить рандомный текст
                OutputArea.setText(output);     //Вставляю текст в area
            }
            case 3 -> { //Если код 3 то свой текст
                output = SetText(3); //выбор своего текста
                OutputArea.setText(output);
            }
            case 4 -> { //Если код 4 то мини-текст1
                output = SetText(4); // выбор текста для мини игры 1
                OutputArea.setText(output);
            }
            case 5 -> { //Если код 5 то мини-текст2
                output = SetText(5); // выбор текста для мини игры 2
                OutputArea.setText(output);
            }
            default -> OutputArea.setText("unreachable");
        }
        //Загрузка текста

    }

}
