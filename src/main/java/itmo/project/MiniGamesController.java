package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import itmo.project.Sound.ButtonSound;
import itmo.project.Texts.SettingsOfTexts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class MiniGamesController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button MiniGame1Button;

    @FXML
    private Button MiniGame2Button;

    @FXML
    private Button MiniGame3Button;

    @FXML
    private Button ReturnButton;

    @FXML
    void initialize() {

        MiniGame3Button.setDisable(true);//3 игра не готова

        MiniGame1Button.setOnAction(actionEvent -> {
            ButtonSound.playMelody();  //звук кнопки

            SettingsOfTexts.typeOfText = 4; //  4- тип первой мини-игры

            MiniGame1Button.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainPlayScene.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });

        MiniGame2Button.setOnAction(actionEvent -> {
            ButtonSound.playMelody();  //звук кнопки

            SettingsOfTexts.typeOfText = 5; //  5- тип второй мини-игры

            MiniGame2Button.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainPlayScene.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });

        ReturnButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            ReturnButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));



            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });


    }

}
