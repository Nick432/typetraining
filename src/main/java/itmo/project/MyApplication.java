package itmo.project;

import itmo.project.UserData.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class MyApplication extends Application {
    public static final ObservableList<User> userDatasApp = FXCollections.observableArrayList();
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MyApplication.class.getResource("MAIN.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1600, 900);
        stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
        stage.setTitle("Тренировка слепой печати");
        stage.setScene(scene);
        stage.show();


    }
    public static void main(String[] args) {
        //ButtonSound.playMain(); уже разъела мозг
        launch();

    }
}