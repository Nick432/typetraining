package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import itmo.project.Sound.ButtonSound;
import itmo.project.Texts.SettingsOfTexts;
import itmo.project.Texts.SmallTexts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RandomTextMenuController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button BigTextButton;

    @FXML
    private Button MediumTextButton;

    @FXML
    private Button ReturnButton;

    @FXML
    private Button SmallTextButton;

    @FXML
    void initialize() {

        ReturnButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            ReturnButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });


        SmallTextButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            SettingsOfTexts.typeOfText = 0; //Выбор малого текста
            SmallTextButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainPlayScene.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });

        MediumTextButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            SettingsOfTexts.typeOfText = 1; //Выбор среднего текста
            MediumTextButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainPlayScene.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });

        BigTextButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            SettingsOfTexts.typeOfText = 2; //Выбор большого текста
            BigTextButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainPlayScene.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();

        });
    }

}
