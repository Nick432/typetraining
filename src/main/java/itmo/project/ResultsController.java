package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;

import itmo.project.Sound.ButtonSound;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ResultsController {

    public static String output = "null"; //Текст, который надо писать
    public static String input = "null"; //Текст, который я написал
    public static long time = 0; //Затраченное время



    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text AccurancyText;

    @FXML
    private Text Time;

    @FXML
    private Button MenuButton;

    @FXML
    private Text NumOfCorrectlyPrintWordsText;

    @FXML
    private Text PrintSpeedText;

    @FXML
    private Button ReturnButton;

    @FXML
    private Button StatsButton;

    @FXML
    private Text TotalWordsInTheTextText;

    @FXML
    void initialize() {

        //Кол-во слов всего в тексте
        int count = 0;
        String[] words = output.split("[\\pP\\s&&[^’]]");
        count = words.length;

        TotalWordsInTheTextText.setText("Всего слов в тексте:" + " " + count);
        //Кол-во слов всего в тексте


        //Время
        Time.setText(String.valueOf("Затраченное время: " + time + " сек."));
        //Время

        //Точность и кол-во правильных слов

            float accurancy = -1;
            int wrongs = 0;
            int min = 0;

            String[] inputWords = input.split("[\\pP\\s&&[^’]]");
            String[] outputWords = output.split("[\\pP\\s&&[^’]]");

            min = Math.min(inputWords.length, outputWords.length);

            for (int i = 0; i < min; i++) {
                if (!(inputWords[i].equals(outputWords[i]))) {
                    wrongs++;
                }
            }

            wrongs = wrongs + Math.abs(inputWords.length-outputWords.length);

            accurancy = (float) (outputWords.length - wrongs) / outputWords.length * 100;
            int correctWords = outputWords.length - wrongs;

        AccurancyText.setText("Точность: " + accurancy + "%");
        NumOfCorrectlyPrintWordsText.setText("Количество правильно напечатанных слов: " + correctWords);


        //Точность и кол-во правильных слов

        //Скорость печати
        float speed = 0;
        if (!(Objects.equals(input, "null"))) {  //чтобы при пустом вводе было скорость пучати - 0.
            speed = (inputWords.length * 60) / time;
        }
        PrintSpeedText.setText("Скорость печати: " + speed + " слов/мин.");
        //Скорость печати

        //Кнопки

        MenuButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            MenuButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        float finalAccurancy = accurancy;
        int finalSpeed = (int) speed;
        StatsButton.setOnAction(event -> {

            //Сохранение данных

            //Дата
            Date curDate = new Date();
            StatsController.dataVar = String.format("%tF %n", curDate);

            //Точность
            StatsController.accurancyVar = finalAccurancy;

            //Скорость
            StatsController.typeSpeedVar = finalSpeed;

            //Правильные слова
            StatsController.numOfCorrectWordsVar = correctWords;

            //Кол-во слов
            StatsController.numOfWordsInTheTextVar = words.length;

            //Время
            StatsController.timeVar = (int) time;

            //Сохранение данных

            ButtonSound.playMelody();  //звук кнопки
            StatsButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Stats.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        ReturnButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            ReturnButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });

        //Кнопки
    }

}
