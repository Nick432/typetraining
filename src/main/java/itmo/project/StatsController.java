package itmo.project;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import itmo.project.Sound.ButtonSound;
import itmo.project.UserData.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class StatsController {

    //Переменные для заполнения таблицы
    public static String dataVar = "default";
    public static float accurancyVar = -100.0f;
    public static int typeSpeedVar = -100;
    public static int numOfCorrectWordsVar = -100;
    public static int numOfWordsInTheTextVar = -100;
    public static int timeVar = - 100;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button ReturnButton;

    @FXML
    private Button exportDataButton;

    @FXML
    private TableView<User> statsTable;

    @FXML
    void initialize() {
        exportDataButton.setDisable(true);  //Не работает еще
        ObservableList<User> userDatas = MyApplication.userDatasApp;

            //База данных
        if (!Objects.equals(dataVar, "default")) {
            MyApplication.userDatasApp.add(new User(dataVar, accurancyVar, typeSpeedVar, numOfCorrectWordsVar, numOfWordsInTheTextVar, timeVar));
        }    //База данных
               //Создал колонки
               TableColumn<User, String> dateColumn = new TableColumn<>("Дата");
               dateColumn.setMinWidth(160);
               TableColumn<User, Float> accuracyColumn = new TableColumn<>("Точность");
               accuracyColumn.setMinWidth(160);
               TableColumn<User, Integer> typeSpeedColumn = new TableColumn<>("Скорость печати");
               typeSpeedColumn.setMinWidth(160);
               TableColumn<User, Integer> numOfCorrectWordsColumn = new TableColumn<>("Кол-во правильных слов");
               numOfCorrectWordsColumn.setMinWidth(160);
               TableColumn<User, Integer> numOfWordsInTheTextColumn = new TableColumn<>("Кол-во слов в тексте");
               numOfWordsInTheTextColumn.setMinWidth(160);
               TableColumn<User, Integer> timeColumn = new TableColumn<>("Затраченное время");
               timeColumn.setMinWidth(160);
               //Создал колонки

               //Настройка ячеек
               dateColumn.setCellValueFactory(new PropertyValueFactory<>("data"));
               accuracyColumn.setCellValueFactory(new PropertyValueFactory<>("accuracy"));
               typeSpeedColumn.setCellValueFactory(new PropertyValueFactory<>("typeSpeed"));
               numOfCorrectWordsColumn.setCellValueFactory(new PropertyValueFactory<>("numOfCorrectWords"));
               numOfWordsInTheTextColumn.setCellValueFactory(new PropertyValueFactory<>("numOfWordsInTheText"));
               timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
               //Настройка ячеек

               //Добавление в таблицу
               statsTable.getColumns().add(dateColumn);
               statsTable.getColumns().add(accuracyColumn);
               statsTable.getColumns().add(typeSpeedColumn);
               statsTable.getColumns().add(numOfCorrectWordsColumn);
               statsTable.getColumns().add(numOfWordsInTheTextColumn);
               statsTable.getColumns().add(timeColumn);
               statsTable.setItems(userDatas); //добавление данных

            //Добавление в таблицу

        ReturnButton.setOnAction(event -> {
            ButtonSound.playMelody();  //звук кнопки
            ReturnButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MAIN.fxml"));


            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image("file:src/main/resources/itmo/project/icons/icon.png"));
            stage.setTitle("Тренировка слепой печати");
            stage.show();
        });


    }

}
