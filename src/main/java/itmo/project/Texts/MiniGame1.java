package itmo.project.Texts;

public class MiniGame1 {

    public static StringBuilder getRandomText() {
        int amount = 10 + (int)Math.floor(Math.random() * 100 ); //случайная длина текста от 10 до 110

       StringBuilder text = new StringBuilder();

       for (int i = 0; i < amount; i++) {  //заполнение случайными буквами текст от А до я
           char randomLetter = (char) ('А' + Math.random() * ('я'-'А' + 1));
           text.append(randomLetter);
           if (i % 5 == 0) {
               text.append(" "); // добавление пробела
           }
       }

        //отдавать
        return text;
    }

}
