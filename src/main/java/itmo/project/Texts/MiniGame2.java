package itmo.project.Texts;

public class MiniGame2 {
    public static StringBuilder getRandomText() {
    int amount = 10 + (int)Math.floor(Math.random() * 100 ); //случайная длина текста от 10 до 110

    StringBuilder text = new StringBuilder();

       for (int i = 0; i < amount; i++) {  //заполнение случайными буквами текст !"№;%:?*()_+/,1234567890-=\.
           char rs = '@'; //randomSymbol
           while (rs == '@' || rs == '#' || rs == '$' || rs == '^' || rs == '&' || rs == '{' || rs == '[' || rs == '}' || rs == ']' ||
                   rs == '\'' || rs == '<' || rs == '>') { // убрать символы, которые требует переключения раскладки с rus  на eng
               rs = (char) ('!' + Math.random() * ('@'-'!' + 1));
           }
        text.append(rs);
        if (i % 2 == 0) {
            text.append(" "); // добавление пробела
        }
    }

    //отдавать
        return text;
    }
}
