package itmo.project.Texts;

import java.util.ArrayList;

public class SettingsOfTexts {
       public static int typeOfText = 0; // 0 - small, 1 - med, 2 - big, 3 - свой текст, 4 - mini-game1, 5 - mini-game2
       public static int arrays = 10; // Кол-во элементов в каждом массиве
       static ArrayList<String> small = new ArrayList<>();
       static ArrayList<String> medium = new ArrayList<>();
       static ArrayList<String> big = new ArrayList<>();

       public static String myText = null; //Свой текст

    public static String SetText(int code) {  //Выбор рандомного текста

        int n = (int)Math.floor(Math.random() * arrays); // рандомное число (arrays - длина массивов)
        String text =  "";
        switch (code) {
            case 0 -> {
                SmallTexts.createSmallText();  //создание массива
                text = small.get(n); //выбор рандомного текста из массива
            }
            case 1 -> {
                MediumTexts.createMediumText(); //создание массива
                text = medium.get(n); //выбор рандомного текста из массива
            }
            case 2 -> {
               BigTexts.createBigText(); //создание массива
                text = big.get(n); //выбор рандомного текста из массива
            }
            case 3 -> {
                text = myText; //выбор своего текста
            }
            case 4 -> {
                text = String.valueOf(MiniGame1.getRandomText());
            }
            case 5 -> {
                text = String.valueOf(MiniGame2.getRandomText());
            }
            default -> {
                System.out.println("Ошибка! в SizeOfRandomText");
            }
        }

        return text;
    }

}
