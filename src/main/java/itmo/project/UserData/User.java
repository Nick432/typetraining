package itmo.project.UserData;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class User {

    public SimpleStringProperty data;
    public SimpleFloatProperty accuracy;
    public SimpleIntegerProperty typeSpeed;
    public SimpleIntegerProperty numOfCorrectWords;
    public SimpleIntegerProperty numOfWordsInTheText;
    public SimpleIntegerProperty time;

    public User(String data, float accuracy, int typeSpeed, int numOfCorrectWords, int numOfWordsInTheText, int time){
        this.data = new SimpleStringProperty(data);
        this.accuracy = new SimpleFloatProperty(accuracy);
        this.typeSpeed = new SimpleIntegerProperty(typeSpeed);
        this.numOfWordsInTheText = new SimpleIntegerProperty(numOfWordsInTheText);
        this.numOfCorrectWords = new SimpleIntegerProperty(numOfCorrectWords);
        this.time = new SimpleIntegerProperty(time);
    }

    public String getData() {
        return data.get();
    }

    public SimpleStringProperty dataProperty() {
        return data;
    }

    public void setData(String data) {
        this.data.set(data);
    }

    public float getAccuracy() {
        return accuracy.get();
    }

    public SimpleFloatProperty accuracyProperty() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy.set(accuracy);
    }

    public int getTypeSpeed() {
        return typeSpeed.get();
    }

    public SimpleIntegerProperty typeSpeedProperty() {
        return typeSpeed;
    }

    public void setTypeSpeed(int typeSpeed) {
        this.typeSpeed.set(typeSpeed);
    }

    public int getNumOfCorrectWords() {
        return numOfCorrectWords.get();
    }

    public SimpleIntegerProperty numOfCorrectWordsProperty() {
        return numOfCorrectWords;
    }

    public void setNumOfCorrectWords(int numOfCorrectWords) {
        this.numOfCorrectWords.set(numOfCorrectWords);
    }

    public int getNumOfWordsInTheText() {
        return numOfWordsInTheText.get();
    }

    public SimpleIntegerProperty numOfWordsInTheTextProperty() {
        return numOfWordsInTheText;
    }

    public void setNumOfWordsInTheText(int numOfWordsInTheText) {
        this.numOfWordsInTheText.set(numOfWordsInTheText);
    }

    public int getTime() {
        return time.get();
    }

    public SimpleIntegerProperty timeProperty() {
        return time;
    }

    public void setTime(int time) {
        this.time.set(time);
    }
}