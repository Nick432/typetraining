module itmo.project {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    opens itmo.project to javafx.fxml;
    exports itmo.project;
    exports itmo.project.Sound;
    opens itmo.project.Sound to javafx.fxml;
    opens itmo.project.UserData; //ИЗ-ЗА ЭТОГО ТАБЛИЦА НЕ ГРУЗИЛА ДАННЫЕ!
}